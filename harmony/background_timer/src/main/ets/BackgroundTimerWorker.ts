/*
*
* Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
* Use of this source code is governed by a MIT license that can be
* found in the LICENSE file
*
*/

import worker, { MessageEvents } from '@ohos.worker';

const workerPort = worker.workerPort;

workerPort.onmessage = (e: MessageEvents): void => {
  // e : MessageEvents, 用法如下：
  let data = e.data;
  data.rnInstance.emitDeviceEvent("backgroundTimer.timeout", null)
}
